﻿namespace LR6_FORM
{
    partial class Form
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.sourceTextBox = new System.Windows.Forms.TextBox();
            this.tailTextBox = new System.Windows.Forms.TextBox();
            this.button = new System.Windows.Forms.Button();
            this.headTextBox = new System.Windows.Forms.TextBox();
            this.delimeterTextBox = new System.Windows.Forms.TextBox();
            this.headTailComponent = new LR6_DLL.HeadTailComponent(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 25);
            this.label1.TabIndex = 3;
            this.label1.Text = "Head";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(28, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 25);
            this.label2.TabIndex = 5;
            this.label2.Text = "Tail";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sourceTextBox
            // 
            this.sourceTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sourceTextBox.Location = new System.Drawing.Point(12, 12);
            this.sourceTextBox.Name = "sourceTextBox";
            this.sourceTextBox.Size = new System.Drawing.Size(395, 31);
            this.sourceTextBox.TabIndex = 0;
            // 
            // tailTextBox
            // 
            this.tailTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tailTextBox.Location = new System.Drawing.Point(81, 123);
            this.tailTextBox.Name = "tailTextBox";
            this.tailTextBox.ReadOnly = true;
            this.tailTextBox.Size = new System.Drawing.Size(391, 31);
            this.tailTextBox.TabIndex = 6;
            this.tailTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button
            // 
            this.button.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button.Location = new System.Drawing.Point(12, 49);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(460, 31);
            this.button.TabIndex = 2;
            this.button.Text = "DO SOME MAGIC";
            this.button.UseVisualStyleBackColor = true;
            this.button.Click += new System.EventHandler(this.button_Click);
            // 
            // headTextBox
            // 
            this.headTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.headTextBox.Location = new System.Drawing.Point(81, 86);
            this.headTextBox.MaxLength = 1;
            this.headTextBox.Name = "headTextBox";
            this.headTextBox.ReadOnly = true;
            this.headTextBox.Size = new System.Drawing.Size(391, 31);
            this.headTextBox.TabIndex = 4;
            this.headTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // delimeterTextBox
            // 
            this.delimeterTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.delimeterTextBox.Location = new System.Drawing.Point(413, 12);
            this.delimeterTextBox.Name = "delimeterTextBox";
            this.delimeterTextBox.Size = new System.Drawing.Size(59, 31);
            this.delimeterTextBox.TabIndex = 1;
            this.delimeterTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 165);
            this.Controls.Add(this.delimeterTextBox);
            this.Controls.Add(this.tailTextBox);
            this.Controls.Add(this.headTextBox);
            this.Controls.Add(this.sourceTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form";
            this.Text = "LR6 (Custom component)";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LR6_DLL.HeadTailComponent headTailComponent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox sourceTextBox;
        private System.Windows.Forms.TextBox delimeterTextBox;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.TextBox headTextBox;
        private System.Windows.Forms.TextBox tailTextBox;
    }
}

