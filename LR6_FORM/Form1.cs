﻿using System;

namespace LR6_FORM
{
    public partial class Form : System.Windows.Forms.Form
    {
        public Form()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            headTextBox.Text = headTailComponent.GetHead(sourceTextBox.Text, delimeterTextBox.Text[0]);
            tailTextBox.Text = headTailComponent.GetTail(sourceTextBox.Text, delimeterTextBox.Text[0]);
        }
    }
}