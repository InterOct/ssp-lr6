﻿using System.ComponentModel;
using System.Linq;

namespace LR6_DLL
{
    public partial class HeadTailComponent : Component
    {
        public HeadTailComponent()
        {
            InitializeComponent();
        }

        public HeadTailComponent(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        public string GetHead(string text, char delimeter)
        {
            return text.Contains(delimeter) ? text.Substring(0, text.IndexOf(delimeter)) : text;
        }

        public string GetTail(string text, char delimeter)
        {
            var pos = text.IndexOf(delimeter);
            return text.Contains(delimeter) ? text.Substring(pos + 1, text.Length - pos - 1) : text;
        }
    }
}